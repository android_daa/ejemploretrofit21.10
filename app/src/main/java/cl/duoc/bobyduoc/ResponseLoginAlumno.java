package cl.duoc.bobyduoc;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LC1300XXXX on 21/10/2017.
 */

public class ResponseLoginAlumno {
    @SerializedName("data")
    private Alumno alumno;

    public ResponseLoginAlumno() {
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
}
