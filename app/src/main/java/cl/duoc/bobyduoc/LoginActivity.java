package cl.duoc.bobyduoc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private Button btnEntrar;
    private EditText etUsuario, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnEntrar = (Button)findViewById(R.id.btnLogin);
        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUsuario(etUsuario.getText().toString(), etPassword.getText().toString());
            }
        });
    }

    private void loginUsuario(String nombre, String clave){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        Gson gson = new GsonBuilder()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://vivo.duoc.cl/VivoMobileServer/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        BobyDuocService service = retrofit.create(BobyDuocService.class);
        Call<ResponseLoginAlumno> repos = service.loginAlumno(nombre,
                clave);
        repos.enqueue(new Callback<ResponseLoginAlumno>() {
            @Override
            public void onResponse(Call<ResponseLoginAlumno> call, Response<ResponseLoginAlumno> response) {
                if (response.code() == 200) {
                    Alumno values = response.body().getAlumno();
                    if(!values.getDetalleError().isEmpty()){
                        Toast.makeText(LoginActivity.this,values.getDetalleError() , Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(LoginActivity.this, "Nombre: " + values.getNombreCompleto(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseLoginAlumno> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
