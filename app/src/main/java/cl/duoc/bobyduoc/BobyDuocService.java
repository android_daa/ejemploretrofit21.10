package cl.duoc.bobyduoc;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by LC1300XXXX on 21/10/2017.
 */

public interface BobyDuocService {
    //?usuariofdsfds={usuario}&password={password}
    @GET("loginAlumno")
    Call<ResponseLoginAlumno> loginAlumno(@Query("usuario") String nombreUsuario, @Query("password") String passwordUsuario);
}
