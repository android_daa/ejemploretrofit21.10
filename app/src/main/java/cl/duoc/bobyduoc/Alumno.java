package cl.duoc.bobyduoc;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LC1300XXXX on 21/10/2017.
 */

public class Alumno {
    @SerializedName("email_duoc")
    private String emailDuoc;
    @SerializedName("email_personal")
    private String emailPersona;
    @SerializedName("rut")
    private String rut;
    @SerializedName("nombre_completo")
    private String nombreCompleto;

    @SerializedName("detalle")
    private String detalleError;

    public Alumno() {
    }

    public String getEmailDuoc() {
        return emailDuoc;
    }

    public void setEmailDuoc(String emailDuoc) {
        this.emailDuoc = emailDuoc;
    }

    public String getEmailPersona() {
        return emailPersona;
    }

    public void setEmailPersona(String emailPersona) {
        this.emailPersona = emailPersona;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDetalleError() {
        return detalleError;
    }

    public void setDetalleError(String detalleError) {
        this.detalleError = detalleError;
    }
}
